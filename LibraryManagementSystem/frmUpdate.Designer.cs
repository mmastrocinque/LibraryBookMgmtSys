﻿namespace LibraryManagementSystem
{
    partial class frmUpdate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbID = new System.Windows.Forms.TextBox();
            this.btnFetch = new System.Windows.Forms.Button();
            this.tbDue = new System.Windows.Forms.TextBox();
            this.tbAuthorFirst = new System.Windows.Forms.TextBox();
            this.tbAuthorLast = new System.Windows.Forms.TextBox();
            this.tbTitle = new System.Windows.Forms.TextBox();
            this.tbCheckedOut = new System.Windows.Forms.TextBox();
            this.tbLocation = new System.Windows.Forms.TextBox();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.gbFields = new System.Windows.Forms.GroupBox();
            this.gbFields.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbID
            // 
            this.tbID.Location = new System.Drawing.Point(20, 21);
            this.tbID.Name = "tbID";
            this.tbID.Size = new System.Drawing.Size(163, 20);
            this.tbID.TabIndex = 1;
            this.tbID.Text = "ID";
            this.tbID.TextChanged += new System.EventHandler(this.tbID_TextChanged);
            // 
            // btnFetch
            // 
            this.btnFetch.Location = new System.Drawing.Point(189, 21);
            this.btnFetch.Name = "btnFetch";
            this.btnFetch.Size = new System.Drawing.Size(75, 52);
            this.btnFetch.TabIndex = 8;
            this.btnFetch.Text = "Fetch Entry First";
            this.btnFetch.UseVisualStyleBackColor = true;
            this.btnFetch.Click += new System.EventHandler(this.btnFetch_Click);
            // 
            // tbDue
            // 
            this.tbDue.Location = new System.Drawing.Point(8, 95);
            this.tbDue.Name = "tbDue";
            this.tbDue.Size = new System.Drawing.Size(163, 20);
            this.tbDue.TabIndex = 6;
            this.tbDue.Text = "Due By";
            // 
            // tbAuthorFirst
            // 
            this.tbAuthorFirst.Location = new System.Drawing.Point(8, 69);
            this.tbAuthorFirst.Name = "tbAuthorFirst";
            this.tbAuthorFirst.Size = new System.Drawing.Size(163, 20);
            this.tbAuthorFirst.TabIndex = 4;
            this.tbAuthorFirst.Text = "Author_First";
            // 
            // tbAuthorLast
            // 
            this.tbAuthorLast.Location = new System.Drawing.Point(8, 44);
            this.tbAuthorLast.Name = "tbAuthorLast";
            this.tbAuthorLast.Size = new System.Drawing.Size(163, 20);
            this.tbAuthorLast.TabIndex = 3;
            this.tbAuthorLast.Text = "Author_Last";
            // 
            // tbTitle
            // 
            this.tbTitle.Location = new System.Drawing.Point(8, 18);
            this.tbTitle.Name = "tbTitle";
            this.tbTitle.Size = new System.Drawing.Size(163, 20);
            this.tbTitle.TabIndex = 2;
            this.tbTitle.Text = "Title";
            // 
            // tbCheckedOut
            // 
            this.tbCheckedOut.Enabled = false;
            this.tbCheckedOut.Location = new System.Drawing.Point(20, 125);
            this.tbCheckedOut.Name = "tbCheckedOut";
            this.tbCheckedOut.Size = new System.Drawing.Size(163, 20);
            this.tbCheckedOut.TabIndex = 5;
            this.tbCheckedOut.Text = "Last Checked Out";
            // 
            // tbLocation
            // 
            this.tbLocation.Location = new System.Drawing.Point(8, 121);
            this.tbLocation.Name = "tbLocation";
            this.tbLocation.Size = new System.Drawing.Size(163, 20);
            this.tbLocation.TabIndex = 7;
            this.tbLocation.Text = "Location";
            // 
            // btnUpdate
            // 
            this.btnUpdate.Enabled = false;
            this.btnUpdate.Location = new System.Drawing.Point(189, 99);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 52);
            this.btnUpdate.TabIndex = 9;
            this.btnUpdate.Text = "Update Fetched Entry";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // gbFields
            // 
            this.gbFields.Controls.Add(this.tbTitle);
            this.gbFields.Controls.Add(this.tbAuthorLast);
            this.gbFields.Controls.Add(this.tbLocation);
            this.gbFields.Controls.Add(this.tbAuthorFirst);
            this.gbFields.Controls.Add(this.tbDue);
            this.gbFields.Enabled = false;
            this.gbFields.Location = new System.Drawing.Point(12, 47);
            this.gbFields.Name = "gbFields";
            this.gbFields.Size = new System.Drawing.Size(171, 149);
            this.gbFields.TabIndex = 10;
            this.gbFields.TabStop = false;
            this.gbFields.Text = "Fields to edit";
            // 
            // frmUpdate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(289, 208);
            this.Controls.Add(this.gbFields);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.tbCheckedOut);
            this.Controls.Add(this.tbID);
            this.Controls.Add(this.btnFetch);
            this.Name = "frmUpdate";
            this.Text = "frmUpdate";
            this.gbFields.ResumeLayout(false);
            this.gbFields.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbID;
        private System.Windows.Forms.Button btnFetch;
        private System.Windows.Forms.TextBox tbDue;
        private System.Windows.Forms.TextBox tbAuthorFirst;
        private System.Windows.Forms.TextBox tbAuthorLast;
        private System.Windows.Forms.TextBox tbTitle;
        private System.Windows.Forms.TextBox tbCheckedOut;
        private System.Windows.Forms.TextBox tbLocation;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.GroupBox gbFields;
    }
}