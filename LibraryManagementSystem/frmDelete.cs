﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace LibraryManagementSystem
{
    public partial class frmDelete : Form
    {
        public frmDelete()
        {
            InitializeComponent();
        }

        private void BookID_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void btnDeleteEntryByID_Click(object sender, EventArgs e)
        {
            string connectionString = @"Data Source=..\..\Resources\LibraryBooksDB.db; 
Version=3; FailIfMissing=True;";

            try
            {
                using (SQLiteConnection conn = new SQLiteConnection(connectionString))
                {
                    conn.Open();
                    SQLiteCommand delSQL = new SQLiteCommand("DELETE FROM LibraryBooks WHERE ID=@ID", conn);
                    delSQL.Parameters.AddWithValue("@ID", tbID.Text);
                    try
                    {
                        if(delSQL.ExecuteNonQuery() == 0)
                        {
                            MessageBox.Show("Delete Failed, check your ID!");
                        }
                    }
                    catch (SQLiteException ex)
                    {

                        throw new SQLiteException(ex.Message);
                    }
                    conn.Close();
                }

            }
            catch (SQLiteException ex)
            {
                throw new SQLiteException(ex.Message);
            }
        }

    }
}
