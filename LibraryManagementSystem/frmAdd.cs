﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace LibraryManagementSystem
{
    public partial class frmAdd : Form
    {
        public frmAdd()
        {
            InitializeComponent();
        }

        private void frmAdd_Load(object sender, EventArgs e)
        {
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string connectionString = @"Data Source=..\..\Resources\LibraryBooksDB.db; 
Version=3; FailIfMissing=True;";
            if (tbID.Text.IsNumeric())
            {
                try
                {
                    using (SQLiteConnection conn = new SQLiteConnection(connectionString))
                    {
                        conn.Open();
                        SQLiteCommand insertSQL = new SQLiteCommand("INSERT INTO LibraryBooks (ID,Title, Author_Last, Author_First, Current_Location) VALUES (@ID,@Title,@Author_Last,@Author_First,@Location)", conn);
                        insertSQL.Parameters.AddWithValue("@ID", tbID.Text);
                        insertSQL.Parameters.AddWithValue("@Title", tbTitle.Text);
                        insertSQL.Parameters.AddWithValue("@Author_Last", tbAuthorLast.Text);
                        insertSQL.Parameters.AddWithValue("@Author_First", tbAuthorFirst.Text);
                        insertSQL.Parameters.AddWithValue("@Location", tbLocation.Text);
                        try
                        {
                            insertSQL.ExecuteNonQuery();
                        }
                        catch (SQLiteException)
                        {

                            MessageBox.Show("ERROR: Check your ID, it must be unique!");
                        }
                        conn.Close();
                    }

                }
                catch (SQLiteException ex)
                {
                    throw new SQLiteException(ex.Message);
                }
            }
            else
            {
                MessageBox.Show("ID MUST be a unique integer!", "ERROR:INVALID DATA TYPE GIVEN");
            }
        }
    }
    public static class StringExtensions
    {
        public static bool IsNumeric(this string input)
        {
            int number;
            return int.TryParse(input, out number);
        }
    }
}
