﻿namespace LibraryManagementSystem
{
    partial class frmDelete
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDeleteEntryByID = new System.Windows.Forms.Button();
            this.tbID = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnDeleteEntryByID
            // 
            this.btnDeleteEntryByID.Location = new System.Drawing.Point(33, 61);
            this.btnDeleteEntryByID.Name = "btnDeleteEntryByID";
            this.btnDeleteEntryByID.Size = new System.Drawing.Size(153, 54);
            this.btnDeleteEntryByID.TabIndex = 1;
            this.btnDeleteEntryByID.Text = "Delete Book by ID";
            this.btnDeleteEntryByID.UseVisualStyleBackColor = true;
            this.btnDeleteEntryByID.Click += new System.EventHandler(this.btnDeleteEntryByID_Click);
            // 
            // tbID
            // 
            this.tbID.Location = new System.Drawing.Point(60, 35);
            this.tbID.Name = "tbID";
            this.tbID.Size = new System.Drawing.Size(100, 20);
            this.tbID.TabIndex = 2;
            this.tbID.Text = "BookID";
            // 
            // frmDelete
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(223, 149);
            this.Controls.Add(this.tbID);
            this.Controls.Add(this.btnDeleteEntryByID);
            this.Name = "frmDelete";
            this.Text = "Delete Entry";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnDeleteEntryByID;
        private System.Windows.Forms.TextBox tbID;
    }
}