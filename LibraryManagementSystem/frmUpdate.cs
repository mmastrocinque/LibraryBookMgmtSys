﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace LibraryManagementSystem
{
    public partial class frmUpdate : Form
    {
        public frmUpdate()
        {
            InitializeComponent();
        }

        private void btnFetch_Click(object sender, EventArgs e)
        {
            string connectionString = @"Data Source=..\..\Resources\LibraryBooksDB.db; 
Version=3; FailIfMissing=True;";

            try
            {
                using (SQLiteConnection conn = new SQLiteConnection(connectionString))
                {
                    conn.Open();
                    SQLiteCommand findSQL = new SQLiteCommand("SELECT * FROM LibraryBooks WHERE ID= :id",conn);
                    findSQL.Parameters.AddWithValue("id", tbID.Text);
                    SQLiteDataReader DR = findSQL.ExecuteReader();
                    try
                    {
                        btnUpdate.Enabled = false;
                        while(DR.Read())
                        {
                            tbTitle.Text = DR[1] as string ?? "";
                            tbAuthorLast.Text = DR[2] as string ?? "";
                            tbAuthorFirst.Text = DR[3] as string ?? "";
                            tbCheckedOut.Text = DR[4] as string ?? "";
                            tbDue.Text = DR[5] as string ?? "";
                            tbLocation.Text = DR[6] as string ?? "";
                            btnUpdate.Enabled = true;
                            gbFields.Enabled = true;
                        }

                    }
                    catch (SQLiteException ex)
                    {
                        throw new SQLiteException(ex.Message);

                    }
                    conn.Close();
                }

            }
            catch (SQLiteException ex)
            {
                throw new SQLiteException(ex.Message);
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            string connectionString = @"Data Source=..\..\Resources\LibraryBooksDB.db; 
Version=3; FailIfMissing=True;";

            try
            {
                using (SQLiteConnection conn = new SQLiteConnection(connectionString))
                {
                    conn.Open();
                    SQLiteCommand updateSQL = new SQLiteCommand("UPDATE LibraryBooks SET Title = :title, Author_Last = :aLast, Author_First = :afirst, Last_Checked_Out = :lco, Due_By = :due, Current_Location =:loc  WHERE ID=:id", conn);
                    updateSQL.Parameters.AddWithValue("title", tbTitle.Text);
                    updateSQL.Parameters.AddWithValue("alast", tbAuthorLast.Text);
                    updateSQL.Parameters.AddWithValue("afirst", tbAuthorFirst.Text);
                    updateSQL.Parameters.AddWithValue("lco", tbCheckedOut.Text);
                    updateSQL.Parameters.AddWithValue("due", tbDue.Text);
                    updateSQL.Parameters.AddWithValue("loc", tbLocation.Text);
                    updateSQL.Parameters.AddWithValue("id", tbID.Text);
                    try
                    {
                        updateSQL.ExecuteNonQuery();

                    }
                    catch (SQLiteException ex)
                    {
                        throw new SQLiteException(ex.Message);

                    }
                    conn.Close();
                }

            }
            catch (SQLiteException ex)
            {
                throw new SQLiteException(ex.Message);
            }
        }

        private void tbID_TextChanged(object sender, EventArgs e)
        {
            btnUpdate.Enabled = false;
            gbFields.Enabled = false;
        }
    }
}
