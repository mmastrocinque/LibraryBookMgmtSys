﻿namespace LibraryManagementSystem
{
    partial class frmAdd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbTitle = new System.Windows.Forms.TextBox();
            this.tbAuthorLast = new System.Windows.Forms.TextBox();
            this.tbAuthorFirst = new System.Windows.Forms.TextBox();
            this.tbLocation = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.tbID = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // tbTitle
            // 
            this.tbTitle.Location = new System.Drawing.Point(12, 38);
            this.tbTitle.Name = "tbTitle";
            this.tbTitle.Size = new System.Drawing.Size(163, 20);
            this.tbTitle.TabIndex = 0;
            this.tbTitle.Text = "Title";
            // 
            // tbAuthorLast
            // 
            this.tbAuthorLast.Location = new System.Drawing.Point(12, 64);
            this.tbAuthorLast.Name = "tbAuthorLast";
            this.tbAuthorLast.Size = new System.Drawing.Size(163, 20);
            this.tbAuthorLast.TabIndex = 1;
            this.tbAuthorLast.Text = "Author_Last";
            // 
            // tbAuthorFirst
            // 
            this.tbAuthorFirst.Location = new System.Drawing.Point(12, 90);
            this.tbAuthorFirst.Name = "tbAuthorFirst";
            this.tbAuthorFirst.Size = new System.Drawing.Size(163, 20);
            this.tbAuthorFirst.TabIndex = 2;
            this.tbAuthorFirst.Text = "Author_First";
            // 
            // tbLocation
            // 
            this.tbLocation.Location = new System.Drawing.Point(12, 116);
            this.tbLocation.Name = "tbLocation";
            this.tbLocation.Size = new System.Drawing.Size(163, 20);
            this.tbLocation.TabIndex = 3;
            this.tbLocation.Text = "Location";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(186, 64);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 46);
            this.button1.TabIndex = 4;
            this.button1.Text = "Add Entry";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tbID
            // 
            this.tbID.Location = new System.Drawing.Point(12, 12);
            this.tbID.Name = "tbID";
            this.tbID.Size = new System.Drawing.Size(163, 20);
            this.tbID.TabIndex = 5;
            this.tbID.Text = "ID";
            // 
            // frmAdd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(273, 165);
            this.Controls.Add(this.tbID);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.tbLocation);
            this.Controls.Add(this.tbAuthorFirst);
            this.Controls.Add(this.tbAuthorLast);
            this.Controls.Add(this.tbTitle);
            this.Name = "frmAdd";
            this.Text = "Add new Entry";
            this.Load += new System.EventHandler(this.frmAdd_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbTitle;
        private System.Windows.Forms.TextBox tbAuthorLast;
        private System.Windows.Forms.TextBox tbAuthorFirst;
        private System.Windows.Forms.TextBox tbLocation;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox tbID;
    }
}