﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace LibraryManagementSystem
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
            loadDataBase();
        }

        private void loadDataBase()
        {

            string connectionString = @"Data Source=..\..\Resources\LibraryBooksDB.db; 
Version=3; FailIfMissing=True;";

            try
            {
                using (SQLiteConnection conn = new SQLiteConnection(connectionString))
                {


                    conn.Open();
                    SQLiteCommand sql_cmd = conn.CreateCommand();
                    sql_cmd.CommandText = "SELECT * FROM LibraryBooks";
                    SQLiteDataAdapter DB = new SQLiteDataAdapter(sql_cmd.CommandText, conn);
                    DataTable DS = new DataTable();
                    DB.Fill(DS);
                    dGBooks.DataSource = DS;
                    dGBooks.Show();
                    conn.Close();
                    
                }

            }
            catch (SQLiteException ex)
            {
                throw new SQLiteException(ex.Message);
            }

        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            loadDataBase();
        }

        
        private void btnAdd_Click(object sender, EventArgs e)
        {
           frmAdd addForm = new frmAdd();
            addForm.Show();
            
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            frmDelete delForm = new frmDelete();
            delForm.Show();

        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            frmUpdate updateForm = new frmUpdate();
            updateForm.Show();
        }
        
    }
}
